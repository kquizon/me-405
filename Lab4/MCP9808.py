'''
@file MCP9808.py
@brief This class is a driver for the MCP9808 Temperature Sensor.
@details This class acts as a driver for the MCP9808 Ambient Temperature sensor
         when that sensor is connected to a Nucleo L476 Dev Board via pins
         SCL and SDA for I2C Communication. The class features three accessible methods.
         'Check' checks the communicated manufacuter's ID against the published ID
         for the MCP9808. Check returns true if the IDs match and false if the IDs do not.
         'Celsius' returns a float of the temperature read by the MCP9808 in degrees Celsius.
         'Farenheit' returns a float of the temperature read by the MCP9808 in degrees Farenheit.
@author Kai Quizon
@date February 11th, 2021
'''

#THE ADDRESS FOR MY MCP9808 IS 24


from pyb import I2C
import utime #Only imported for test case

class MCP9808:
    '''
    @brief This class acts as a driver for the MCP9808. See \ref MCP9808.py for details.
    '''
    
    def __init__(self, address):
        '''
        @brief Initiates the class by storing a copy of desired address and intiating I2C communication
        @param address The Device address for the connected MCP9808
        '''
        ## Create I2C on Bus 1 and initiate as Master
        self.i2c = I2C(1, I2C.MASTER)
        self.addr = address
        
    def check(self):
        '''
        @brief This function checks whether the read value matches the Manufacturer ID of MCP9808.
        @return A boolean True or False. True if check passes, False if check fails.
        '''
        ## Buffer Array to write from Manufacturer ID Register
        checkbuf = bytearray(2)
        
        #Write into checkbuf from manufacturer ID register
        self.i2c.mem_read(checkbuf, addr = self.addr, memaddr = 6)
        
        #Check if Published Manufacturer ID matches received ID
        if checkbuf[1] == 0x0054:
            return True
        else:
            return False
        
    def celsius(self):
        '''
        @brief Returns the temperature read by the MCP9808 in Degrees Celsius
        @return A float (decimal) value for the temperature read in Degrees Celsius.
        '''
        ## Buffer Array to write Temperature Data Into
        buf = bytearray(2)
        
        #Write Temperature Data to buf from corresponding register on MCP9808
        self.i2c.mem_read(buf, addr = self.addr, memaddr = 5)
        
        #Clear Flags
        buf[0] = buf[0] & 0x1F
        # Check if data is below 0*C
        if buf[0] & 0x10 == 0x10:
            # If data is below 0*C, clear sign and "resign" in integer calc
            buf[0] = buf[0] & 0x0F
            temp = (buf[0]*16+buf[1]/16)-256   #Converts to temperature in decimal
        else:    
            # Runs if data indicates above 0*C
            temp = buf[0]*16+buf[1]/16      #Converts to temperature in decimal
        return temp
    
    def farenheit(self):
        '''
        @brief Returns the temperature read by the MCP9808 in Degrees Farenheit.
        @return A float (decimal) value for the temperature read in Degrees Farenheit.
        '''
        buf = bytearray(2)
        self.i2c.mem_read(buf, addr = self.addr, memaddr = 5)
        buf[0] = buf[0] & 0x1F
        # Check if data is below 0*C
        if buf[0] & 0x10 == 0x10:
            # If data is below 0*C, clear sign and "resign" in integer calc
            buf[0] = buf[0] & 0x0F
            temp = (buf[0]*16+buf[1]/16)-256
            
        else:    
            # Runs if data indicates above 0*C
            temp = buf[0]*16+buf[1]/16
        tempF = temp*(9/5)+32
        return tempF
    
    
if __name__ == '__main__':
    #This function returns the temperature in degrees celsius every second (if the driver is running correctly)
    mcp = MCP9808(24)
    #Check that Manufacturer ID matches published.
    mcp.check()
    while True:
        print(mcp.celsius())
        utime.sleep(1)
            