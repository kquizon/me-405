#ADC Test

import pyb

button = pyb.Pin(pyb.Pin.board.PC13, pyb.Pin.OUT_PP)
button.low()

ADCReader = pyb.ADC(pyb.Pin.board.A5)

while True:
    print(ADCReader.read())