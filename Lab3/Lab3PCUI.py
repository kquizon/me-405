'''
@file    Lab3PCUI.py
@brief   This file governs the PC user interface for initiating data collection and graphing.
@details This file awaits a user keystroke ("g") to initiate data collection on the nucleo running \ref Lab3Nuke.py.
         It sends this initiation character over the serial port. The file then awaits
         a data array over the serial port. When an array is transmitted, the data is
         plotted using matplotlib. An example of the output is shown below. @image html ADCVoltageOutputGraphFinal.png width=600in
@author  Kai Quizon
@date    February 4, 2021
'''


from matplotlib import pyplot
import serial
import time
import csv

## Initialize serial port
ser = serial.Serial(port='COM10', baudrate = 115273,timeout=1)

print('''
      Welcome! This script allows you to prompt the collection
      of data related to the voltage response on the Nucleo L476
      to the User Pushbutton. To initiate data collection, press "g"
      ''')
      
## User key input
char = input('Press g to open data collection: ')

#Loop for preventing incorrect inputs
while True:
    if char != 'g':
        char = input('Invalid input. Please press g to open data collection: ')
    else:
        break
    
ser.write(str(char).encode('ascii'))
    

#Loop to wait for returned arrays from nucleo post collection
while True:
    if ser.in_waiting != 0:

        ## Data variables being formatted into lists for plotting containing ADC data generated on the Nucleo
        data = ser.readline().decode('ascii').split('  ')
        out = [m.strip("array('H', [])\n").split(', ') for m in data]
        dat_out = [int(t)*(3.3/4095) for t in out[0]]
        time_stamp = [int(t) for t in out[1]]
        
        #Plotting Data
        pyplot.plot(time_stamp, dat_out)
        pyplot.xlabel('Time (ms)')
        pyplot.title('ADC Digitally Converted Voltage Response to User Pushbutton')
        pyplot.ylabel('ADC Voltage Output (V)')
        
        #Creating CSV File
        filename = 'ADC Voltage Output'+time.strftime('%Y%m%d-%H%M%S')+'.csv'
        with open(filename, 'w', newline='') as csvfile:
            spamwriter = csv.writer(csvfile)
            for n in range(len(time_stamp)):
                spamwriter.writerow([time_stamp[n], dat_out[n]])
            

        break