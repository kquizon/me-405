#UART test

import pyb
from pyb import UART

uart = UART(2)

while True:
    if uart.any() != 0:
        val = uart.readchar()
        uart.write('You sent an ASCII ' + str(val) + ' to the Nucleo.')
        break
