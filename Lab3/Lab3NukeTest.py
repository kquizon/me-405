from pyb import UART
import array
import pyb

pyb.enable_irq   #Allows for interrupt

## UART Initialization
uart = UART(2)

## Buffer Array for ADC
buffy = array.array('H', (0 for index in range(200)))

## Pre-Buffer Array for ADC to insure push button has been caught correctly
prebuffy = array.array('H', (0 for index in range(20)))

## Time Array for ADC
time = array.array('H', (0 for index in range(200)))

## Push Button Pin
Button = pyb.Pin(pyb.Pin.board.PA0, mode=pyb.Pin.IN)

## ADC On Button Pin
adc = pyb.ADC(Button)

## Timer Running at 100 Hz
tim6 = pyb.Timer(6, freq = 200000)

## Boolean Flag indicating whether interrupt has run
boolflag = None       
#Interrupt Function
def pushed(Line):
    global boolflag
    boolflag = 1
        
## Initialize Interrupt on Falling Edge of User Button
extint = pyb.ExtInt(pyb.Pin.board.PC13,
                    pyb.ExtInt.IRQ_FALLING, 
                    pyb.Pin.PULL_NONE, 
                    callback=pushed)
            
while True:
    if boolflag == 1:
        print('Interrupt Successful')
        boolflag = 0
        char = None
        break
  
while True:
  #Pre-read the ADC and insure that the signal will be caught
  adc.read_timed(prebuffy, tim6)
  if prebuffy[19] != 0:
      print('Prebuffer Successful')
      break

#True read the ADC and send array across serial to PCUI
adc.read_timed(buffy, tim6)
print('Buffer Successful')
uart.write(buffy)
