'''
@file    Lab3Nuke.py
@brief   This file executes the Nucleo back end for ACD and data collection for Lab 3.
@details This file awaits a user keystroke ("g") from \ref Lab3PCUI.py to initiate data collection on the nucleo.
         When a valid key stroke is entered on the PCUI side, the interrupt allows routing
         to the data collection stage. Data is then collected and stored locally on the nucleo
         after being converted to a digital signal by the ADC module. The finished data is then
         transmitted across the serial port for post processing on the PCUI side. ***NOTE: During
         production, this file was named main.py to allow for operation on the Nucleo at startup.
@author  Kai Quizon
@date    February 4, 2021
'''

from pyb import UART
import array
import pyb

pyb.enable_irq   #Allows for interrupt

## Variables for Running ADC Calcs
freq = 200000
ranger = 500
## UART Initialization
uart = UART(2)

## Buffer Array for ADC
buffy = array.array('H', (0 for index in range(ranger)))

## Pre-Buffer Array for ADC to insure push button has been caught correctly
prebuffy = array.array('H', (0 for index in range(20)))

## Time Data Array for ADC collection
time_data = array.array('H', (int(i*(ranger/freq)*1e3) for i in range(ranger)))

## Push Button Pin
Button = pyb.Pin(pyb.Pin.board.PC0, mode=pyb.Pin.IN)

## ADC On Button Pin
adc = pyb.ADC(Button)

## Timer Running at 200000 Hz
tim6 = pyb.Timer(6, freq = 200000)

## Boolean Flag indicating whether interrupt has succesfully run
boolflag = None
 
#Interrupt Function
def pushed(Line):
    global boolflag
    global char
    if char == 'g':
        boolflag = 1
        
## Initialize Interrupt on Falling Edge of User Button
extint = pyb.ExtInt(pyb.Pin.cpu.C13, pyb.ExtInt.IRQ_FALLING, pyb.Pin.PULL_NONE, callback=pushed)
           
while True:
    if uart.any() != 0:
        char = uart.read().decode('ascii')
    elif boolflag == 1:
        boolflag = 0
        char = None
        break
  
while True:
  #Pre-read the ADC and insure that the signal will be caught
  adc.read_timed(prebuffy, tim6)
  if prebuffy[-1] >= 10:
      break

#True read the ADC and send array across serial to PCUI
adc.read_timed(buffy, tim6)
uart.write(str(buffy) + '  ' + str(time_data)+ '\n')

            
