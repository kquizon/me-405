'''
@file Lab2.py
@brief This file tests a user's response time using the Nucleo onboard pushbutton and LED.
@details This file uses interrupt and try except framework to test a user's response time.
         The file is structured in micropython and designed for execution on the Nucleo
         L476 Dev board. When the user starts the program, the LED will randomly light
         between 2 and 3 seconds later. This will continue until the users raises a 
         Keyboard Exception. At this point, the average response time is printed and
         the program is exited.
@author Kai Quizon
@date January 28th, 2021
'''

import utime
import random
import pyb
pyb.enable_irq   #Allows for interrupt

#Declare Global Variables
global n

# Initialize switch, timers, and pins
pinA5 = pyb.Pin (pyb.Pin.cpu.A5, mode=pyb.Pin.OUT_PP)    #Initialize LED pin
pinPC13 = pyb.Pin(pyb.Pin.board.PC13, mode=pyb.Pin.IN)   #Initialize Pushbutton pin
tim2 = pyb.Timer(2, prescaler = 79, period = 0x7FFFFFFF) #Initialize Timer w/ prescalar
tim5 = pyb.Timer(5, prescaler = 79, period = 0x7FFFFFFF) #Initialize Timer w/ prescalar

start_time = utime.ticks_us()

#Preallocate time lists (starting with ten entires) and interval
reacts = [0]
n=0

def pushed(line):
    '''
    @brief This function calls when the pushbutton is depressed and records the appropriate time.
    @param line This param allows for functionality of the interrupt code multiple times.
    '''
    #This is the interrupt function that will be called
    if reacts[n] == 0:               #If the LED has been lit and replaced, allow for further modification
        reacts[n] = tim2.counter()   #Record reaction time value
        
        
    else:
        print('Ah-Ah-Ah! Wait for the LED.')
    
    
#Initialize Interrupt
extint = pyb.ExtInt(pinPC13, pyb.ExtInt.IRQ_FALLING, pyb.Pin.PULL_UP, callback=pushed)    

#Main Loop
while True:
    try:
        if len(reacts)>n:
            #This is the main loop executed each iteration until the keyboard exception is raised
            variance = random.randint(1,1000) #Generate random integer
            delay = 2+variance/1000           #Apply random integer to LED delay
            tim5.counter(0)                   #Prevent triggering of delay before LED is illuminated
            utime.sleep(delay)                #Pause for duration of delay
            pinA5.high()                      #Turn on LED
            tim2.counter(0)                   #'Start' Timer
            utime.sleep(1)                    #Leave LED on for one second regardless of pushbutton
            pinA5.low()                       #Turn off LED
            n=len(reacts)
            
        #This section handles increasing the list length if the most recent interrupt filled the preallocated list
        elif reacts[len(reacts)-1] != 0:
            reacts.append(0)
    except KeyboardInterrupt:
        #SUM AND AVERAGE ALL REACT TIMES
        #EXIT PROGRAM
        print(reacts)
        aresponse = sum(reacts)/(len(reacts)-1)
        print('Your average response time is: ' + str(aresponse) + ' microseconds.')
        quit()