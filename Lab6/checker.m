close all
z = 0.8;
wn = 12;

r = roots([1 2*z*wn wn^2])

r2 = [real(r)*10 real(r)*10.8]

r2(2,:) = [];
disp(r2)

sys = zpk([], [r' r2], 1)

step(sys)