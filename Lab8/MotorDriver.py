'''
@file MotorDriver.py
@author Kai Quizon (adapted from code from Charlie Revfam)
@brief A class to control a motor object with a PCB interface to the Nucleo L476 Dev Board
'''

import pyb

class MotorDriver:
    '''
    @brief This class controls a motor object for the ME 3/405 PCB interfacing with the Nucleo L476 Dev board.
    '''
    
    def __init__(self, nSLEEP_pin, IN1_pin, IN2_pin, timer, motnum=1):
        '''
        Creates a motor driver by initializing GPIO pins and turning the motor
        off for safety.
        @param nSLEEP_pin   A pyb.Pin object to use as the enable pin. MUST be input as a string.
        @param IN1_pin      A pyb.Pin object to use as the input to half bridge 1. MUST be input as a string.
        @param IN2_pin      A pyb.Pin object to use as the input to half bridge 2. MUST be input as a string.
        @param timer        A pyb.Timer object to use for PWM generation on IN1_pin and IN2_pin.
        @param motnum       An idenitfying number for the motor, either 1 or 2. This allows for 1 timer to operate both motors.
        '''
        
        ## The defining motor number (used to run two different motors off the same timer.)
        self.motnum = motnum
        ## The sleeper pin
        self.sleeper = pyb.Pin(nSLEEP_pin, pyb.Pin.OUT_PP)
        ## Pin 1 of the active motor pins
        pin1 = pyb.Pin(IN1_pin)
        ## Pin 2 of the active motor pins
        pin2 = pyb.Pin(IN2_pin)
        tim = pyb.Timer(timer, freq=20000)
        #This next section of code handles distributing channels based on motor number.
        if self.motnum == 1:
            ## The two channels for pulse width modulation control
            self.ch1 = tim.channel(1, pyb.Timer.PWM, pin=pin1)
            self.ch2 = tim.channel(2, pyb.Timer.PWM, pin=pin2)
        elif self.motnum == 2:
            ## The two channels for pulse width modulation control
            self.ch1 = tim.channel(3, pyb.Timer.PWM, pin=pin1)
            self.ch2 = tim.channel(4, pyb.Timer.PWM, pin=pin2)
        else:
            print('Motnum can only be either 1 or 2. Use a different timer to control more than 2 motors.')
        
        self.ch1.pulse_width_percent(0)
        self.ch2.pulse_width_percent(0)
        self.sleeper.low()
        print('Created a Motor Driver Object')
        
    def enable(self):
        '''
        @brief Enables the motor for motion.
        '''
        print('Enabling Motor')
        self.sleeper.high()
        
    def disable(self):
        '''
        @brief Disables the motor, preventing further motion.

        '''
        print('Disabling Motor')
        self.sleeper.low()
        
    def set_duty(self, duty):
        '''
        @param duty   A scalar value between -100 and 100 to control the % pulse width modulation
        '''
        if duty < 0:
            self.ch1.pulse_width_percent(abs(duty))
            self.ch2.pulse_width_percent(0)
        elif duty >= 0:
            self.ch2.pulse_width_percent(duty)
            self.ch1.pulse_width_percent(0)


if __name__ == '__main__':
    
    # Create motor objects
    mot_1 = MotorDriver('A15', 'B4', 'B5', 3, 1)
    mot_2 = MotorDriver('A15', 'B0', 'B1', 3, 2)

    # Enable the motors  and zero faults
    mot_1.enable()
    mot_2.enable()
    fault = 0
    
    def faultthrow(line):
        '''
        @brief This function calls when the nFault pin, pin B2, is low, indicating an overcurrrent condition in the motor.
        '''
        global fault
        mot_1.disable()
        mot_2.disable()
        fault = 1
        
    # Define external interupt to stop motors if there is a fault according to
    # the motor's internal fault pin.
    extint = pyb.ExtInt(pyb.Pin(pyb.Pin.board.PB2, mode=pyb.Pin.IN),
                        pyb.ExtInt.IRQ_FALLING, 
                        pyb.Pin.PULL_NONE, 
                        callback = faultthrow)

    while True:
        try:
            if fault == 0:
                #print('Spinning Motors...')
                # spin motor 1 clockwise at 85% speed
                mot_1.set_duty(85)
                # spin motor 2 counterclockwise at 85% speed
                mot_2.set_duty(-85)
                
                In = ''
            
            elif fault == 1:
                extint.disable()
                # User interface to deal with motor faults
                if In != 'Fault Cleared':
                    print('A fault has been detected. Please check your hardware and type "Fault Cleared" when the issue is resolved.')
                    In = input('Type "Fault Cleared" to resume motors.\n')
                    
                else: 
                    print('Resuming...')
                    mot_1.enable()
                    mot_2.enable()
                    
                    fault = 0
                    extint.enable()
            else:
                pass #Error handling
            
        except KeyboardInterrupt:
            mot_1.set_duty(0)
            mot_2.set_duty(0)
            mot_1.disable()
            mot_2.disable()
            print('Motors have been stopped and disabled.')
            break
                
    
    
