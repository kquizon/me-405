'''
@file Lab1.py
@brief   This file utilizes a finite state machine to operate a virtual vending machine.
@details This file contains a script that runs a virtual vending machine 
         called Vendotron. After being greeted by a startup message, the
         user is able to select a preferred beverage at any time. The user
         can also input coins or bills. The script will determine if the
         correct change has been provided, and return a drink, or
         an insufficient funds message. At any point the user can eject their
         balance. The state machine governing the vending machines access is shown below:
             @image html Lab1SD.png width=600in
@author Kai Quizon
@date January 20, 2021
'''

import keyboard

# Initialize Variables and Lists
state = 0
last_key = ''
global price
global name

balance = 0

def kb_cb(key):
    ''' 
    @brief This function runs whenever an applicable key is pressed.
    '''
    global last_key
    last_key = key.name
    

# Tell the keyboard module to respond to these particular keys only
keyboard.on_release_key("c", callback=kb_cb)
keyboard.on_release_key("p", callback=kb_cb)
keyboard.on_release_key("0", callback=kb_cb)
keyboard.on_release_key("1", callback=kb_cb)
keyboard.on_release_key("2", callback=kb_cb)
keyboard.on_release_key("3", callback=kb_cb)
keyboard.on_release_key("4", callback=kb_cb)
keyboard.on_release_key("5", callback=kb_cb)
keyboard.on_release_key("s", callback=kb_cb)
keyboard.on_release_key("d", callback=kb_cb)
keyboard.on_release_key("e", callback=kb_cb)


def getChange(price, payment):
    '''
    @brief This function calculates change in the fewest coins possible for a given price and payment scheme.
    @details This file contains a function that takes the price of an item as an integer in cents and
             a tuple containing the payment method in twenties, tens, fives, ones, quarters, dimes, nickles, and pennies
             and returns a tuple containing the number of twenties, tens, fives, ones, quarters,
             dimes, nickels, and pennies that represent the fewest pieces for the change off an
             item.
    @param price An integer value representing the price in cents (ex: $4.57 = 457)
    @param payment An integer representing the payment issued times one hundred ($10.00 = 1000).
    @return Returns a tuple and an integer. The tuple represents change in the fewest number of coins starting with pennies of the left and ascending.
            The integer value is the change vended multiplied by one hundred.
    '''

    # Convert Payment to integer value
    paid = payment
         
    # Compute Change Loop
    diff = paid - price
    changeval = diff
    change = [0, 0, 0, 0, 0, 0, 0, 0]
    while diff > 0:
        if diff%2000 == 0:
            change[7] += 1
            diff = diff-2000
        elif diff%1000 == 0:
            change[6] += 1
            diff = diff-1000
        elif diff%500 == 0:
            change[5] += 1
            diff = diff-500    
        elif diff%100 == 0:
            change[4] += 1
            diff = diff-100
        elif diff%25 == 0:
            change[3] += 1
            diff = diff-25
        elif diff%10 == 0:
            change[2] += 1
            diff = diff-10
        elif diff%5 == 0:
            change[1] += 1
            diff = diff-5
        elif diff%1 == 0:
            change[0] += 1
            diff = diff-1
    
    change = tuple(change)
    
    return change, changeval

def printWelcome():
    '''
    @brief This function prints the Vendotron Welcome Statement
    '''
    print('''
    Vendotron Welcomes You! As a measly human you have the following options:
        0: Deposit a Penny
        1: Deposit a Nickle
        2: Deposit a Dime
        3: Deposit a Quarter
        4: Deposit One Dollar
        5: Deposit Five Dollars
        C: Vend a Cuke for $1.00
        P: Vend a Popsi for $1.20
        S: Vend a Spryte for $0.85
        D: Vend a Dr. Pupper for $1.10
        E: Eject all available funds
    ''')

    print('Your current balance is: $' + format((balance/100), '.2f'))

while True:
    '''
    Runs Finite State Machine show here: \image html 
    '''
    # Runs state 0 Code - Initialization State
    if state == 0:
        printWelcome()
        state = 1
        
    # Runs state 1 Code - Holding State
    elif state == 1:
        
        # Hub State Distribution
        if last_key == '0' or last_key == '1' or last_key == '2' or last_key == '3' or last_key == '4' or last_key == '5':
            state = 2
            
        elif last_key == 'd' or last_key == 'c' or last_key == 's' or last_key == 'p':
            state = 3
            
        elif last_key == 'e':
            state = 5
            
        
    # Runs state 2 Code - Addition State
    elif state == 2:
        if last_key == '0':
            balance += 1
            print('Your current balance is: $' + format((balance/100), '.2f'))
            last_key = ''
        elif last_key == '1':
            balance += 5
            print('Your current balance is: $' + format((balance/100), '.2f'))
            last_key = ''
        elif last_key == '2':
            balance += 10
            print('Your current balance is: $' + format((balance/100), '.2f'))
            last_key = ''
        elif last_key == '3':
            balance += 25
            print('Your current balance is: $' + format((balance/100), '.2f'))
            last_key = ''
        elif last_key == '4':
            balance += 100
            print('Your current balance is: $' + format((balance/100), '.2f'))
            last_key = ''
        elif last_key == '5':
            balance += 500
            print('Your current balance is: $' + format((balance/100), '.2f'))
            last_key = ''
            
        #Always return to state 1    
        state = 1
        
    # Runs state 3 Code - PreVend State, gets Drink Name & Price
    elif state == 3:

        # Get Drink Price and Name
        if last_key == 'p':
            price = 120
            name = 'Popsi'
            last_key = ''
        elif last_key == 's':
            price = 85
            name = 'Spryte'
            last_key = ''
        elif last_key == 'd':
            price = 110
            name = 'Dr. Popper'
            last_key = ''
        elif last_key == 'c':
            price = 100
            name = 'Cuke'
            last_key = ''
            
        state = 4
        
        
    # Runs state 4 Code - Vend state, if allowed, vends drink and change
    elif state == 4:
        if balance >= price:
            print('----'+name+' VENDED----')
            balance = balance - price
            print('Your current balance is: $' + format((balance/100), '.2f'))
            print('''Try another refreshing beverage?
                      0: Deposit a Penny
                      1: Deposit a Nickle
                      2: Deposit a Dime
                      3: Deposit a Quarter
                      4: Deposit One Dollar
                      5: Deposit Five Dollars
                      C: Vend a Cuke for $1.00
                      P: Vend a Popsi for $1.20
                      S: Vend a Spryte for $0.85
                      D: Vend a Dr. Pupper for $1.10
                      E: Eject remaining balance
                  ''')
            state = 1
        elif balance < price:
            print('Your payment is insufficient peasant. Please insert more coinage')
            print(name+'costs '+ format((price/100), '.2f'))
            last_key = ''
            state = 1

        
    # Runs state 5 Code - Vend State, Ejects Item and Change
    elif state == 5:
        
        (change, changeval) = getChange(0, balance)
        print('You received $'+ format((changeval/100), '.2f') + ' in change.')
        print('You received '+str(change[0]) + ' pennies.')
        print('You received '+str(change[1]) + ' nickels.')
        print('You received '+str(change[2]) + ' dimes.')
        print('You received '+str(change[3]) + ' quarters.')
        print('You received '+str(change[4]) + ' dollar bills.')
        print('You received '+str(change[5]) + ' five dollar bills.')
        
        last_key = ''
        
        state = 1
        
    ## Error Handling
    else:
        pass