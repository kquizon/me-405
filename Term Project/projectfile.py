
'''
@author Anil Singh
@author Kai Quizon
@brief
@details

@date Sat Mar 13 14:22:21 2021

'''

import pyb
from micropython import const, alloc_emergency_exception_buf
import gc
import utime
import machine

import cotask
import task_share
#import print_task

from touchdriver import touchdriver
from MotorDriver import MotorDriver
from Encoder import Encoder
from bno055 import BNO055

# Allocate memory so that exceptions raised in interrupt service routines can
# generate useful diagnostic printouts
alloc_emergency_exception_buf (100)

'''
Shared variables:
    
    Boolean Flags (User Inputs, selections for type of run):
    
        - Run_X           (enable X axis controller)
        - Run_Y           (enable Y axis controller)
        - Enc_IMU         (Toggle determination of thetay (1 for IMU, 0 for enc))
        - Collect_Data    (push data to CSV file for plotting)
    
    State Variables:    
        
        STATE X:
        -X               [x-position of ball]                   (touchdriver)
        -X_dot           [x-velocity of ball]                   (touchdriver)
        -thetay          [y-angle of platform]                  (Encoder/BNO055)
        -thetay_dot      [y-angular velocity of platform]       (Encoder/BNO055)
        
        STATE Y:
        -Y               [y-position of ball]                   (touchdriver)
        -Y_dot           [y-velocity of ball]                   (touchdriver)
        -thetax          [x-angle of platform]                  (Encoder/BNO055)
        -thetay_dot      [x-angular velocity of platform]       (Encoder/BNO055)

'''
def xTable ():
    '''
    @brief Run the X axis controller
    @details
    @return
    '''
    #-------Hardware Setup-------

    motorY.enable()
    
    ## Controller Gains (Calculated In MATLAB)
    K1 = -3.2835
    K2 = 0.2771
    K3 = -9.0898
    K4 = 3.9913
    
    ## Converter from Torque to PWM duty cycle by assuming PWM*T = % of Stall Torque
    Conv = 100/0.112 
    
    #-------Task Execution Loop-------
    while True:
        if fault.get() == 0:
            #Only run if there are values from the sensors in the queue
            if X.any() and ThY.any():
                x = X.get()
                xd = X_dot.get()
                thy = ThY.get()
                thyd = ThY_dot.get()
                
                ## Required torque for correction
                T = K3*x + K4*thy + K1*xd + K2*thyd
                print(x)
                print(xd)
                print(thy)
                print(thyd)
                
                ## PWM Duty Cycle for execution:
                Duty = T*Conv
                
                if Duty>100:
                    Duty = 100
                elif Duty<-100:
                    Duty = -100
                
                ## Command motor to run at that duty cycle
                motorY.set_duty(Duty)
            
        else:
            pass
        
        yield

def yTable ():
    '''
    @brief Run the Y axis controller
    @details
    @return
    '''
    
    motorX.enable()
    
    ## Controller Gains (Calculated In MATLAB)
    K1 = -3.2835
    K2 = 0.2771
    K3 = -9.0898
    K4 = 3.0
    
    ## Converter from Torque to PWM duty cycle by assuming PWM*T = % of Stall Torque
    Conv = 100*2.21/(12*0.0138)
    
    #-------Task Execution Loop-------
    while True:
        if fault.get() == 0:
            #Only run if there are values from the sensors in the queue
            if Y.any() and ThX.any():
                y = Y.get()
                yd = Y_dot.get()
                thx = ThX.get()
                thxd = ThX_dot.get()
                
                ## Required torque for correction
                T = K3*y + K4*thx + K1*yd + K2*thxd
                print(T)
                ## PWM Duty Cycle for execution:
                Duty = T*Conv
                
                if Duty>100:
                    Duty = 100
                elif Duty<-100:
                    Duty = -100
                
                ## Command motor to run at that duty cycle
                motorX.set_duty(Duty)
                print(Duty)
            
        yield    

def chkEnc ():
    '''
    @brief Read from encoders and store data to appropriate queue.
    '''
     # -----Intiialize Hardware Here-----
    
    ency = Encoder('PB6','PB7',4)
    encx = Encoder('PC6','PC7',8)

    delta_time = 0
    degX_last = 0
    degY_last = 0
    
    # Angle conversion factor from crank angle to table angle
    Conv_angle = 0 #CURRENTLY NOT CORRECT
    
    T1 = utime.ticks_us()
    
    # -----Run Task Here-----    
    while True:
            
        T2 = utime.ticks_us()
        delta_time = utime.ticks_diff(T2,T1) 
        
        encx.update()
        ency.update()
        
        degX = Conv_angle*encx.deg_pos
        degY = Conv_angle*ency.deg_pos
        
        ThX.put(degX*3.14/180)
        ThY.put(degY*3.14/180)
        
        ThX_dot.put(((degX-degX_last)/delta_time)*3.14/180)
        ThY_dot.put(((degY-degY_last)/delta_time)*3.14/180)
        
        T1 = utime.ticks_us()
        degX_last = degX
        degY_last = degY
            
        yield

def chkIMU ():
    '''
    @brief Read from IMU and store data to appropriate queue.
    @details
    '''
    # -----   Intiialize Hardware Here   -----
    
    i2c = machine.I2C(1)    
    imu = BNO055(i2c)
    calibrated = False
    
    gyro = 0
    head = 0

    # -----   Run Task Here   -----    
    while True:
        
        if not calibrated:
            calibrated = imu.calibrated()
        gyro = imu.gyro()
        head = imu.euler()
        
        ThX.put(head[1])
        ThX_dot.put(gyro[1])
        
        ThY.put(head[2])
        ThY_dot.put(gyro[0])
            
        yield

def chkTch ():
    '''
    @brief Read from touch screen and store to appropriate queue.
    @details
    '''
    # -----   Intiialize Hardware Here   -----
    
    tch = touchdriver('PA0','PA6','PA1','PA7')
    
    x_last = 0
    y_last = 0
    
    contact = 0
    
    delta_time = 0
    
    T1 = utime.ticks_us()
    
    # -----   Run Task Here   -----
    while True:

        if tch.tch == False:
            tch.zreader()
            contact = 0
        if tch.tch == True:
            tch.read()
            contact = 1
        
        T2 = utime.ticks_us()
        delta_time = utime.ticks_diff(T2,T1)
        
        if contact == 1:
            X.put(tch.pos[0])
            Y.put(tch.pos[1])
            X_dot.put((tch.pos[0] - x_last)/delta_time)
            Y_dot.put((tch.pos[1] - y_last)/delta_time)
        elif contact == 0:
            X.put(0)
            Y.put(0)
            X_dot.put(0)
            Y_dot.put(0)
                  
        T1 = utime.ticks_us()
        x_last = tch.pos[0]
        y_last = tch.pos[1]
        
        yield

def storeData ():
    '''
    Save Data Buffer to CSV
    '''
    #Do we want the CSV file open the entire time within this task? Or specifically
    #Opening and closing it?
    
    while True:
        
        #stuff happens
        pass
    
    pass


if __name__ == '__main__':
    
    '''
    Shared variables:
        
        Boolean Flags (User Inputs, selections for type of run):
        
            - Run_X           (enable X axis controller)
            - Run_Y           (enable Y axis controller)
            - Enc_IMU         (Toggle determination of thetay (1 for IMU, 0 for enc))
            - Collect_Data    (push data to CSV file for plotting)
        
        State Variables:    
            
            STATE X:
            -X               [x-position of ball]                   (touchdriver)
            -X_dot           [x-velocity of ball]                   (touchdriver)
            -thetay          [y-angle of platform]                  (Encoder/BNO055)
            -thetay_dot      [y-angular velocity of platform]       (Encoder/BNO055)
            
            STATE Y:
            -Y               [y-position of ball]                   (touchdriver)
            -Y_dot           [y-velocity of ball]                   (touchdriver)
            -thetax          [x-angle of platform]                  (Encoder/BNO055)
            -thetay_dot      [x-angular velocity of platform]       (Encoder/BNO055)
    
    '''
    ## User Selections
    
    Runx = False                  # Run the X axis controller?
    Runy = True                  # Run the Y axis controller?
    EI = 1                       # 1 for IMU, 0 for Encoder
    C_data = False               # Collect data?
    
    #Initialize the Shares
    '''
    ## Run X Axis Share
    share0 = task_share.Share('i', thread_protect = False, name = 'Run_Xaxis')
    ## Run Y Axis Share
    share1 = task_share.Share('i', thread_protect = False, name = 'Run_Yaxis')
    ## Encoder or IMU? Share
    share2 = task_share.Share('i', thread_protect = False, name = 'Enc_IMU')
    ## Collect Data?
    share3 = task_share.Share('i', thread_protect = False, name = 'Collect_Data')
    '''
    ## Motor Fault
    fault = task_share.Share('i',  name = 'fault')
    
    #Setting up the Queues
    ## X Queues
    ## X Queue
    X = task_share.Queue('f', 8, overwrite=True, name='X')
    ## X_dot Queue
    X_dot = task_share.Queue('f', 8, overwrite=True, name='X_dot')
    ## Theta Y Queue
    ThY = task_share.Queue('f', 8, overwrite=True, name='ThY')
    ## Theta Y_dot Queue
    ThY_dot = task_share.Queue('f', 8, overwrite=True, name='ThY_dot')
    
    ##Y Queues
    ## Y Queue
    Y = task_share.Queue('f', 8, overwrite=True, name='Y')
    ## Y_dot Queue
    Y_dot = task_share.Queue('f', 8, overwrite=True, name='Y_dot')
    ## Theta X Queue
    ThX = task_share.Queue('f', 8, overwrite=True, name='ThX')
    ## Theta X_dot Queue
    ThX_dot = task_share.Queue('f', 8, overwrite=True, name='ThX_dot')
    
    '''
    share0.put(Runx)
    share1.put(Runy)
    share2.put(EI)
    share3.put(C_data)
    '''

    if Runx:
        task1 = cotask.Task (xTable, name = 'xTable', priority = 1, 
                             period = 10)
        cotask.task_list.append (task1)
        
    if Runy:
        task2 = cotask.Task (yTable, name = 'yTable', priority = 12, 
                             period = 10)
        cotask.task_list.append (task2)
    
    if not EI:
        task3 = cotask.Task (chkEnc, name = 'chkEnc', priority = 3, 
                             period = 5)
        cotask.task_list.append (task3)
    
    if EI:
        task4 = cotask.Task (chkIMU, name = 'chkIMU', priority = 3, 
                             period = 3)
        cotask.task_list.append (task4)
        
    task5 = cotask.Task (chkTch, name = 'chkTch', priority = 4,
                         period = 5)
    cotask.task_list.append(task5)
    
    '''
    if C_Data:
        task6 = cotask.Task(Collect_Data, name = 'Collect_Data', priority = 3)
        cotask.task_list.append(task6)
    '''
    motorY = MotorDriver('A15', 'B4', 'B5', 3, 1)
    motorX = MotorDriver('A15', 'B4', 'B5', 3, 1)
    
    ## Define the interrupt function (CONTAINS A SHARE VARIABLE)
    def faultthrow(line):
        '''
        @brief This function calls when the nFault pin, pin B2, is low, indicating an overcurrrent condition in the motor.
        '''
        motorX.disable()
        motorY.disable()
        fault.put(1)
        
    # Define external interupt to stop motors if there is a fault according to
    # the motor's internal fault pin.
    extint = pyb.ExtInt(pyb.Pin(pyb.Pin.board.PB2, mode=pyb.Pin.IN),
                        pyb.ExtInt.IRQ_FALLING, 
                        pyb.Pin.PULL_NONE, 
                        callback = faultthrow)
    
    #Clean up memory before executing tasks
    gc.collect()
    
    #Run the scheduler with Dr. Ridgely's priority scheduling algorithm, quit on
    #any keyboard input.
    vcp = pyb.USB_VCP()
    ## Fault clearing input initial definition
    In = ''
    while not vcp.any():
        if fault.get() == 1:
            extint.disable()
            # User interface to deal with motor faults
            while In != 'Fault Cleared':
                In = input('Type "Fault Cleared" to resume motors if hardware is clear.\n')
    
            motorX.enable()
            motorY.enable()
            
            fault.put(0)
            extint.enable()
                    
                        
        cotask.task_list.pri_sched()
        
    #Empty comm port buffer
    vcp.read()
    
    #Print goodbye message
    print('Hope you had a TILTASTIC time! See ya real soon.')