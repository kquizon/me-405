'''
@file touchdriver.py
@brief A driver that runs an ER-TFT080-1 Resistive Touch Panel. This class was created with partner Rick Hall.
@details A driver that runs an ER-TFT080-1 Resistive Touch Panel. This driver contains fourth methods: 1) a method to read only the x component
         2) a method to read only the y component 3) a method to read only the z component
         4) a method to read all three components (returned as a tuple). This class accounts
         for settling time of the RTP by placing a 5us hold after energizing the pins. The
         values are "filtered" by taking the average of three data points upon collection.
@author Kai Quizon
'''

#This class is essentially a copy of Lab7test, but without the testing code on the bottom and with correct
#conversion factors in calculations.

import pyb
import utime
import array


class touchdriver:
    '''
    @brief A driver that runs an ER-TFT080-1 Resistive Touch Panel. This class was created with partner Rick Hall.
    @details A driver that runs an ER-TFT080-1 Resistive Touch Panel. This driver contains fourth methods: 1) a method to read only the x component
         2) a method to read only the y component 3) a method to read only the z component
         4) a method to read all three components (returned as a tuple). This class accounts
         for settling time of the RTP by placing a 5us hold after energizing the pins. The
         values are "filtered" by taking the average of three data points upon collection.
             
    '''
    
    def __init__(self,xmpin,ympin,xppin,yppin):
        '''
        @brief Initializes the touchdriver class pins and preallocates buffer arrays for the first three methods.
        @param xmpin The pin to which the xm pin of the touchscreen is attached.
        @param ympin The pin to which the ym pin of the touchscreen is attached.
        @param xppin The pin to which the xp pin of the touchscreen is attached.
        @param yppin The pin to which the yp pin of the touchscreen is attached/
        '''
        
        ## Full position array for three positional coordinates
        self.pos = array.array('f',[0, 0, 0])
        ## Boolean Object that reads true when the screen is being touched and false when the screen is not being touched.
        self.tch = False
        
        ## Array that contains the dimensions of the touchscreen (prefilled for ER-TFT080-1 RTP)
        self.Cal = array.array('h',[300, 3820, 480, 3650])
        self.yCal = 107/(self.Cal[3]-self.Cal[2])
        self.xCal = 182/(self.Cal[1]-self.Cal[0])
        self.cCal = [2060,2065]
        
        ## X positional buffer for reading three x positions off RTP
        self.xbuf = array.array('h',[0, 0, 0])
        ## Y positional buffer for reading three y positions off RTP
        self.ybuf = array.array('h',[0, 0, 0])
        ## Z positional buffer for reading three z readings off RTP
        self.zbuf = array.array('h',[0, 0, 0])
        
        ## Pin object of the xm RTP Pin
        self.xm = pyb.Pin(xmpin)       
        ## Pin object of the yp RTP Pin
        self.yp = pyb.Pin(yppin)     
        ## Pin object of the xp RTP Pin
        self.xp = pyb.Pin(xppin)     
        ## Pin object of the ym RTP Pin
        self.ym = pyb.Pin(ympin)        
        
        #Preset pins to dset 4 (chosen just because, no reason for 4)
        self.phigh = pyb.Pin(self.ym, mode=pyb.Pin.OUT_PP)
        self.phigh.high()
        self.plow  = pyb.Pin(self.xp, mode=pyb.Pin.OUT_PP)
        self.plow.low()
            
        self.pfloat = pyb.Pin(self.yp, mode=pyb.Pin.IN)
        self.pread = pyb.ADC(self.xm)
        self.dset = 4
        
    
        
    #METHOD 1: Read the x location ONLY
    def xreader(self):
        '''
        @brief Read the x location of the touch panel ONLY.
        @details This method reads the touch panel's "x" output only. The reading is filtered
                 by taking the average of three readings. Settling time errors are eliminated
                 by introducing a delay of 5 us.
        @return The x value of the touch location in [mm].
        '''
        
        # Reset Pins for x-read in Optimal Configuration
        if self.dset == 2 or self.dset == 4: # Y or Zy
        # If the previous set was Y or Zy, Configure all:
            
            self.phigh = pyb.Pin(self.xm, mode=pyb.Pin.OUT_PP)
            self.phigh.high()
            self.plow  = pyb.Pin(self.xp, mode=pyb.Pin.OUT_PP)
            self.plow.low()
            
            self.pfloat = pyb.Pin(self.ym, mode=pyb.Pin.IN)
            self.pread = pyb.ADC(self.yp)
            self.dset = 1
            
        elif self.dset == 3: # Zx
        # If the previous set was Zx, Configure s.t. ADC & Low already set:

            self.phigh = pyb.Pin(self.xp, mode=pyb.Pin.OUT_PP)
            self.phigh.high()
            # self.plow  = pyb.Pin(self.xm, mode=pyb.Pin.OUT_PP)
            # self.plow.low()
            
            self.pfloat = pyb.Pin(self.ym, mode=pyb.Pin.IN)
            # self.pread = pyb.ADC(self.yp)
            self.dset = 1            
        
        # Read Sensor Data & filter
        utime.sleep_us(5)
        self.xbuf[0] = self.pread.read()
        utime.sleep_us(5)
        self.xbuf[1] = self.pread.read()
        utime.sleep_us(5)
        self.xbuf[2] = self.pread.read()
        self.pos[0] = round((self.xbuf[0]+self.xbuf[1]+self.xbuf[2])/3)

        self.pos[0] = round(self.xCal*(self.pos[0]-self.cCal[0]),2)
        
        return(int(self.pos[0]))
    
    #METHOD 2: Read y position ONLY
    def yreader(self):
        '''
        @brief Read the y location of the touch panel ONLY.
        @details This method reads the touch panel's "x" output only. The reading is filtered
                 by taking the average of three readings. Settling time errors are eliminated
                 by introducing a delay of 5 us.
        @return The y value of the touch location in [mm].
        '''
        
        # Reset Pins for y-read in Optimal Configuration
        if self.dset == 1 or self.dset == 3: # X or Zx
        # If the previous set was X or Zx, Configure all:
                
            self.phigh = pyb.Pin(self.ym, mode=pyb.Pin.OUT_PP)
            self.phigh.high()
            self.plow  = pyb.Pin(self.yp, mode=pyb.Pin.OUT_PP)
            self.plow.low()
            
            self.pfloat = pyb.Pin(self.xp, mode=pyb.Pin.IN)
            self.pread = pyb.ADC(self.xm)
            self.dset = 2
            
        elif self.dset == 4: # Zy
        # If the previous set was Zy, Configure s.t. ADC & High already set:
                            
            # self.phigh = pyb.Pin(self.ym, mode=pyb.Pin.OUT_PP)
            # self.phigh.high()
            self.plow  = pyb.Pin(self.yp, mode=pyb.Pin.OUT_PP)
            self.plow.low()
            
            self.pfloat = pyb.Pin(self.xp, mode=pyb.Pin.IN)
            # self.pread = pyb.ADC(self.xm)
            self.dset = 2

        
        # Read Sensor Data & filter
        utime.sleep_us(5)
        self.ybuf[0] = self.pread.read()
        utime.sleep_us(5)
        self.ybuf[1] = self.pread.read()
        utime.sleep_us(5)
        self.ybuf[2] = self.pread.read()
        self.pos[1] = round((self.ybuf[0]+self.ybuf[1]+self.ybuf[2])/3)
        
        self.pos[1] = round(self.yCal*(self.pos[1]-self.cCal[1]),2)
        
        return(int(self.pos[1]))
        
    #METHOD 3: Read z direction ONLY
    def zreader(self):
        '''
        @brief Read the y location of the touch panel ONLY.
        @details This method reads the touch panel's "x" output only. The reading is filtered
                 by taking the average of three readings. Settling time errors are eliminated
                 by introducing a delay of 5 us.
        @return The z value of the touch location in [mm].
        @return A boolean flag on whether the plate is being contacted or not.
        '''
        
         # Reset Pins for z-read in Optimal Configuration
        if self.dset == 2:
        # If the previous set was Y, Configure s.t. ADC & High already set:
                
            # self.phigh = pyb.Pin(self.ym, mode=pyb.Pin.OUT_PP)
            # self.phigh.high()
            self.plow  = pyb.Pin(self.xp, mode=pyb.Pin.OUT_PP)
            self.plow.low()
            
            self.pfloat = pyb.Pin(self.yp, mode=pyb.Pin.IN)
            # self.pread = pyb.ADC(self.xm)
            self.dset = 4
            
        elif self.dset == 1:
        # If the previous set was X, Configure s.t. ADC & Low already set:
                            
            self.phigh = pyb.Pin(self.ym, mode=pyb.Pin.OUT_PP)
            self.phigh.high()
            # self.plow  = pyb.Pin(self.xp, mode=pyb.Pin.OUT_PP)
            # self.plow.low()
            
            self.pfloat = pyb.Pin(self.xm, mode=pyb.Pin.IN)
            # self.pread = pyb.ADC(self.yp)
            self.dset = 3

        
        # Read Sensor Data & filter
        
        utime.sleep_us(5)
        self.zbuf[0] = self.pread.read()
        utime.sleep_us(5)
        self.zbuf[1] = self.pread.read()
        utime.sleep_us(5)
        self.zbuf[2] = self.pread.read()
        
        self.pos[2] = round((self.zbuf[0]+self.zbuf[1]+self.zbuf[2])/3)
        
        # If ADC reads less than high, or greater than low, touch detected:
        if 16 < self.pos[2] < 4080:
            self.tch = True
        else:
            self.tch = False
    
    #METHOD 4: Read ALL 3 Channels
    def read(self):
        '''
        @brief Read the x,y, and z locations of the touch panel.
        @details This method reads the touch panel's three outputs. The reading is filtered
                 by taking the average of three readings. Settling time errors are eliminated
                 by introducing a delay of 5 us.
        @return The x,y, and z values of the touch location in [mm] as a tuple. Unless there is no contact, then the tuple contains all logical Falses.
        '''
        
        if self.tch == False:
            self.zreader()
            if self.tch == True:
                self.xreader()
                self.yreader()
        else:
            if self.dset == 1:
                self.xreader()
                self.zreader()
                self.yreader()
            else:
                self.yreader()
                self.zreader()
                self.xreader()
                
        return self.pos
    
            
if __name__ == '__main__':
    while True:
        try:
            test = input('\n'*20 + 'What direction would you like to test?\n     - For X direction, enter "x"\n     -For Y direction, enter "y"\n     -For contact test, enter "z"\n     -For full-system test, enter "Full"\n\n                    >>>     ')
            if test != 'x' and test != 'y' and test != 'z' and test != 'Full':
                test = 'Full'

            
            tch = touchdriver('PA0','PA6','PA1','PA7')
            zflag = False
            
            if test == 'x':
                while True:
                    try:
                        t0 = utime.ticks_us()
                        xpos = tch.xreader()
                        
                        t1 = utime.ticks_us()
                        tdif = utime.ticks_diff(t1,t0)
                        
                        print('\n'*20 +' You are touching at: X = ' + str(xpos) + '\n')
                        print('Readtime: ' + str(tdif) + ' us' + '\n'*5)
                        utime.sleep_ms(100)
                    except KeyboardInterrupt:
                        break  
            elif test == 'y':
                while True:
                    try:
                        t0 = utime.ticks_us()
                        ypos = tch.yreader()
                        
                        t1 = utime.ticks_us()
                        tdif = utime.ticks_diff(t1,t0)
                        
                        print('\n'*20 +' You are touching at: Y = ' + str(ypos) + '\n')
                        print('Readtime: ' + str(tdif) + ' us' + '\n'*5)
                        utime.sleep_ms(100)
                    except KeyboardInterrupt:
                        break  
            elif test == 'z':
                while True:
                    try:
                        t0 = utime.ticks_us()
                        [zpos, zflag] = tch.zreader()
                        if zflag == True:
                            t1 = utime.ticks_us()
                            tdif = utime.ticks_diff(t1,t0)
                            
                            print('\n' * 20 + 'You are touching the screen' + '\n')
                            print('Readtime: ' + str(tdif) + ' us' + '\n'*5)
                        else:
                            print('\n' * 20 + 'You are not touching the screen' + '\n'*7)
                        utime.sleep_ms(100)
                    except KeyboardInterrupt:
                        break  
            elif test == 'Full':
                while True:
                    try:
                        
                        if tch.tch == False:
                            t0 = utime.ticks_us()
                            tch.zreader()
                            t1 = utime.ticks_us()
                            tdif = utime.ticks_diff(t1,t0)
                        if tch.tch == True:
                            t0 = utime.ticks_us()
                            position = tch.read()
                            t1 = utime.ticks_us()
                            tdif = utime.ticks_diff(t1,t0)
                            
                            print('\n'*20)
                            
                            # dsp.disp(tch)
                            
                            print('\n'*2 +' You are touching at: X = ' + str(position[0]) +'     Y = ' + str(position[1]) + '     Z = ' + str(position[2]) + ' \n')
                            print('Readtime: ' + str(tdif) + ' us' + '\n'*5)
                        else:
                            print('\n'*20)
                            # dsp.disp(tch)
                            print('\n' * 2 + 'You are not touching the screen' + '\n'*2)
                            print('Readtime: ' + str(tdif) + ' us' + '\n'*5)
                        utime.sleep_ms(100)
                    except KeyboardInterrupt:
                        break  
        except KeyboardInterrupt:
            print('\n'*50 + 'Sucessfuly Exited Test Program \n\n')
            break 