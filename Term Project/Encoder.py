'''
@file Encoder.py
@brief This file contains a class for interacting with a motor encoder using the 
Nucleo Dev board.
@details This file contains a class for interacting with a motor encoder using the 
            Nucleo Dev board. The magnetic encoder uses two channel outputs connected to 
            pins selected by the user on the Nucleo board. Further, the user must specifythe correct timer. 
            To find the correct timer for the corresponding pins, please
            view the Nucleo L746 Dev Board documentaion.
@author Kai Quizon
'''


import pyb

class Encoder:
    '''
    @brief      A class that reads data from a motor encoder.
    @details    This class takes user input to initiate and read data from 
                an encoder, handle over and underflow, and determine angular position of an encoder.
    '''
    

    
    def __init__(self, pin1, pin2, timer, PPR=4000):
        '''
        @brief      Creates an Encoder tied to the specified pins and timer.
        @param pin1 This variable contains the string corresponding to the first pin the motor encoder is connected to.
        @param pin2 This variable contains the string corresponding to the second pin the motor encoder is connecte to.
        @param timer The timer to operate the Encoder off of. This timer will be set up as an ENC_AB mode timer.
        @param PPR   The pulses per revolution of the encoder to be read from. By default set to 4000 for use with ME 3/405 hardware.
        '''
        ## Initialize Timers and Pins
        tim = pyb.Timer(timer)
        tim.init(prescaler=0, period=0xFFFF)
        pinA = pyb.Pin(pin1)
        pinB = pyb.Pin(pin2)
        tim.channel(1, pin=pinA, mode=pyb.Timer.ENC_AB)
        tim.channel(2, pin=pinB, mode=pyb.Timer.ENC_AB)
        
        ## Timer value
        self.tim = tim
        ## Encoder position in pulses
        self.enc_pos = 0 #initialize encoder at zero
        ## Previous encoder position in pulses
        self.enc_prev = 0 #initialize previous encoder value at zero
        ## PPR Value (automatically set to 4000 unless changed by user)
        self.PPR = PPR
        ## Raw encoder position read directly, not used to calculate position, just deltas
        self.encoder_raw = 0
        ## ACTUAL Corrected Delta Value
        self.truedelta = 0
        ## BAD Delta
        self.delta = 0
        ## Degree position
        self.deg_pos = 0
        ## Degree Delta
        self.deg_delta = 0
        
        
       
    def update(self):
        '''
        @brief      Updates the recorded position of the encoder
        @param enc_pos This variable contains the most recent encoder position as updated by the update function.
        '''
        self.enc_prev = self.encoder_raw
        self.encoder_raw = self.tim.counter()
        self.delta = self.encoder_raw-self.enc_prev
        
        self.enc_pos = self.enc_pos + self.get_delta()
        
        self.deg_pos = 360*(self.enc_pos/self.PPR)
        self.deg_delta = 360*(self.truedelta/self.PPR)
        
    def get_position(self):
        '''
        @brief      Returns the value of the encoder
        @return deg_pos Returns the position of the encoder in degrees. The position is only converted to degrees in this variable.
        '''
        
        return int(self.deg_pos)
        
    def set_position(self, new_pos):
        '''
        @brief      Sets the value of the encoder to new_pos
        @param new_pos This variable contains the integer desired to write the encoder position to.
        '''
        self.enc_pos = new_pos
        
        
    def get_delta(self):
        '''
        @brief      Returns the difference between the last two "update" calls.
        '''
        if -0x7FFF < self.delta < (0x7FFF):
            ## self.truedelta is the corrected delta value
            self.truedelta = self.delta
        elif self.delta < -0xFFFF:
            self.truedelta = self.delta+0xFFFF
        elif self.delta > 0xFFFF:
            self.truedelta = self.delta-0xFFFF
        else:
            pass
            
        return int(self.truedelta)
    
    def zero(self):
        '''
        @brief    Set the encoder position to "home" (AKA Zero).
        '''
        self.set_position(0)