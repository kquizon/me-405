
'''
@file projectFile.py
@brief This file calls a scheduler from \ref cotask.py to run a series of tasks cooperatively.
@details This file contains six tasks that are defined and then run cooperatively in a priority
         based multitasking scheme from \ref cotask.py. The six tasks are dependent on four drivers:
         \ref MotorDriver.py , \ref Encoder.py , \ref touchdriver.py , and \ref bno055.py . 
         The tasks are called in a period and priority in order ot control the hardware
         for the term project of ME 405, which is explained in \ref page_TPH An overview
         of the functinality of this software and the data exchanged may be viewed in
         \ref page_TPS

@authors Kai Quizon, Anil Singh
@date Sat Mar 13 14:22:21 2021

'''

import pyb
from micropython import alloc_emergency_exception_buf
import gc
import utime
import machine

import cotask
import task_share
#import print_task

from touchdriver import touchdriver
from MotorDriver import MotorDriver
from Encoder import Encoder
from bno055 import BNO055


# Allocate memory so that exceptions raised in interrupt service routines can
# generate useful diagnostic printouts
alloc_emergency_exception_buf (100)

'''
Shared variables:
    
    Boolean Flags (User Inputs, selections for type of run):
    
        - Run_X           (enable X axis controller)
        - Run_Y           (enable Y axis controller)
        - Enc_IMU         (Toggle determination of thetay (1 for IMU, 0 for enc))
        - Collect_Data    (push data to CSV file for plotting)
    
    State Variables:    
        
        STATE X:
        -X               [x-position of ball]                   (touchdriver)
        -X_dot           [x-velocity of ball]                   (touchdriver)
        -thetay          [y-angle of platform]                  (Encoder/BNO055)
        -thetay_dot      [y-angular velocity of platform]       (Encoder/BNO055)
        
        STATE Y:
        -Y               [y-position of ball]                   (touchdriver)
        -Y_dot           [y-velocity of ball]                   (touchdriver)
        -thetax          [x-angle of platform]                  (Encoder/BNO055)
        -thetay_dot      [x-angular velocity of platform]       (Encoder/BNO055)

'''
def xTable ():
    '''
    @brief Run the X axis controller
    @details Retrieve data from the relevant \ref task_shares.py queues and execute the controller
             based on the data retrieved from the queues. The controller gains are
             derived using characteristic polynomial matching and are tuned using
             trial and error.
    '''
    ## Controller Gains (Calculated In MATLAB)

    K1 = -0.05
    K2 = 0.0015
    K3 = -0.003
    K4 = 3.5
    
    ## Motor Torque Constant (Datasheet)
    kt = .0138  #[Nm/A]
    
    ## Motor Operating Voltage (Datasheet)
    Vdc = 12   #[V]
    
    ## Motor Internal Resistance (Datasheet)
    Rm = 2.21  #[Ohms]
    
    ## Converter from Torque to PWM duty cycle
    Conv = (100*Rm)/(Vdc*kt)
    
    ## Gear Ratio
    GR = 4
    
    ## Convert from degrees to radians
    d2r = 3.14/180
    
    ## Nonlinearity Control
    

    Sat = 51
    stk = 25
    ded = 20
    
    slp = (Sat-stk)/100
    
    #-------Task Execution Loop-------
    while True:
        if fault.get() == 0:
            #Only run if there are values from the sensors in the queue
            if X.any() and ThY.any():
                x = X.get()
                xd = X_dot.get()
                thy = ThY.get()*d2r
                thyd = ThY_dot.get()*d2r
                
                ## Required torque for correction
                T = (K3*x + K4*thy + K1*xd + K2*thyd)/GR

                print(thy/d2r)
                ## PWM Duty Cycle for execution:
                Duty = T*Conv
    
                # Saturation and Nonlinearity to overcome stiction
                if -ded <= Duty <= ded:
                    Duty = 0
                elif ded < Duty <= Sat:
                    Duty = stk + slp*Duty
                elif -Sat <= Duty < -ded:
                    Duty = -stk + slp*Duty
                elif Duty < -Sat:
                    Duty = -Sat
                elif Duty > Sat:
                    Duty = Sat
                    
                ## Command motor to run at that duty cycle
                motorY.set_duty(Duty)
                
        
        
        yield

def yTable ():
    '''
    @brief Run the Y axis controller
    @details Retrieve data from the relevant \ref task_shares.py queues and execute the controller
             based on the data retrieved from the queues. The controller gains are
             derived using characteristic polynomial matching and are tuned using
             trial and error.
    '''

    ## Controller Gains (Calculated In MATLAB)
    K1 = -0.05
    K2 = 0.0015
    K3 = -0.003
    K4 = 1.5
    
    ## Convert from degrees to radians
    d2r = 3.14/180
    
    ## Motor Torque Constant (Datasheet)
    kt = .0138  #[Nm/A]
    
    ## Motor Operating Voltage (Datasheet)
    Vdc = 12   #[V]
    
    ## Motor Internal Resistance (Datasheet)
    Rm = 2.21  #[Ohms]
    
    ## Converter from Torque to PWM duty cycle
    Conv = (100*Rm)/(Vdc*kt)
    
    ## Gear Ratio
    GR = 4
    
    ## Nonlinearity Control
    Sat = 52
    stk = 25
    ded = 20
    
    slp = (Sat-stk)/100
    
    #-------Task Execution Loop-------
    while True:
        if fault.get() == 0:
            #Only run if there are values from the sensors in the queue
            if Y.any() and ThX.any():
                y = Y.get()
                yd = Y_dot.get()
                thx = ThX.get()*d2r
                thxd = ThX_dot.get()*d2r

                #print(y)
                #print(thx/d2r)

                ## Required torque for correction
                T = (K3*y + K4*thx + K1*yd + K2*thxd)/GR

                ## PWM Duty Cycle for execution:
                Duty = T*Conv
                
                '''if Duty>100:
                    Duty = 60
                elif Duty<-100:
                    Duty = -60'''
                    
                # Saturation and Nonlinearity to overcome stiction
                if -ded <= Duty <= ded:
                    Duty = 0
                elif ded < Duty <= Sat:
                    Duty = stk + slp*Duty
                elif -Sat <= Duty < -ded:
                    Duty = -stk + slp*Duty
                elif Duty < -Sat:
                    Duty = -Sat
                elif Duty > Sat:
                    Duty = Sat
                    
                ## Command motor to run at that duty cycle
                motorX.set_duty(Duty)
                
                
            
        yield    

def chkEnc ():
    '''
    @brief Read from encoders using \ref Encoder.py and store data to appropriate queue.
    '''
     # -----Intiialize Hardware Here-----
    
    ency = Encoder('PB6','PB7',4)
    encx = Encoder('PC6','PC7',8)

    delta_time = 0

    
    ## Angle conversion factor from crank angle to table angle
    Conv_angle = 60/110 
    
    T1 = utime.ticks_us()
    
    # -----Run Task Here-----    
    while True:
            
        T2 = utime.ticks_us()
        delta_time = utime.ticks_diff(T2,T1) 
        
        encx.update()
        ency.update()
        
        degX = Conv_angle*encx.deg_pos
        degY = Conv_angle*ency.deg_pos 
        
        ThX.put(degX)
        ThY.put(degY)
        #print(degX)
        

        ThX_dot.put(encx.deg_delta/(delta_time*1E-6))
        ThY_dot.put(ency.deg_delta/(delta_time*1E-6))
        
        T1 = utime.ticks_us()
            
        yield

def chkIMU ():
    '''
    @brief Read from IMU using \ref bno055.py and store data to appropriate queue.
    '''
    # -----   Intiialize Hardware Here   -----
    
    i2c = machine.I2C(1)    
    imu = BNO055(i2c)
    calibrated = False
    
    gyro = 0
    head = 0

    # -----   Run Task Here   -----    
    while True:
        
        if not calibrated:
            calibrated = imu.calibrated()
        gyro = imu.gyro()
        head = imu.euler()
        
        ThX.put(head[1])
        ThX_dot.put(gyro[1])
        
        ThY.put(head[2]-2)
        ThY_dot.put(gyro[0])
            
        yield

def chkTch ():
    '''
    @brief Read from touch screen using \ref touchdriver.py and store to appropriate queue.
    '''
    # -----   Intiialize Hardware Here   -----
    
    tch = touchdriver('PA0','PA6','PA1','PA7')
    
    x_last = 0
    y_last = 0
    
    contact = 0
    
    delta_time = 0
    
    T1 = utime.ticks_us()
    
    # -----   Run Task Here   -----
    while True:

        if tch.tch == False:
            tch.zreader()
            contact = 0
        if tch.tch == True:
            tch.read()
            contact = 1
        
        T2 = utime.ticks_us()
        delta_time = utime.ticks_diff(T2,T1)
        
        if contact == 1:
            X.put(tch.pos[0])
            Y.put(tch.pos[1])
            X_dot.put((tch.pos[0] - x_last)/delta_time)
            Y_dot.put((tch.pos[1] - y_last)/delta_time)
        elif contact == 0:
            X.put(0)
            Y.put(0)
            X_dot.put(0)
            Y_dot.put(0)
                  
        T1 = utime.ticks_us()
        x_last = tch.pos[0]
        y_last = tch.pos[1]
        
        yield

def storeData ():
    '''
    @brief Save ball position data from the touch panel to a CSV file.
    '''
    print('Collect Data Task Entered')
    with open ("csvtouch.csv", "w") as csvFile:
        csvFile.write('Time [s], ThX [deg], ThY [deg], ThXDot [deg/s], ThYdot [deg/s]\n')
        t0 = utime.ticks_us()
        print('Recording Data to CSV >>> ')
        while True:
            xpos = ThX.get()
            ypos = ThY.get()
            xdot = ThX_dot.get()
            ydot = ThY_dot.get()
            t1 = utime.ticks_us()
            tdif = utime.ticks_diff(t1,t0)*1E-6
        
            line = str(tdif) + ', ' + str(xpos) + ', ' + str(ypos) + ', ' + str(xdot) + ', ' + str(ydot) + ', '  + '\n'
            csvFile.write(line)
            print('Data recorded: '+ line)
        
            yield
        
if __name__ == '__main__':
    
    ## Motor X Motor Object using \ref MotorDriver.py
    motorX = MotorDriver('A15', 'B4', 'B5', 3, 1)
    ## Motor Y Motor Object using \ref MotorDriver.py
    motorY = MotorDriver('A15', 'B0', 'B1', 3, 2)
    '''
    Shared variables:
        
        Boolean Flags (User Inputs, selections for type of run):
        
            - Run_X           (enable X axis controller)
            - Run_Y           (enable Y axis controller)
            - Enc_IMU         (Toggle determination of thetay (1 for IMU, 0 for enc))
            - Collect_Data    (push data to CSV file for plotting)
        
        State Variables:    
            
            STATE X:
            -X               [x-position of ball]                   (touchdriver)
            -X_dot           [x-velocity of ball]                   (touchdriver)
            -thetay          [y-angle of platform]                  (Encoder/BNO055)
            -thetay_dot      [y-angular velocity of platform]       (Encoder/BNO055)
            
            STATE Y:
            -Y               [y-position of ball]                   (touchdriver)
            -Y_dot           [y-velocity of ball]                   (touchdriver)
            -thetax          [x-angle of platform]                  (Encoder/BNO055)
            -thetax_dot      [x-angular velocity of platform]       (Encoder/BNO055)
    
    '''
    #User Selections
    
    ## Run X Controller Boolean
    Runx = True                 # Run the X axis controller?
    ## Run Y Controller Boolean
    Runy = True                  # Run the Y axis controller?
    ## Encoder or IMU Boolean
    EI = 1                       # 1 for IMU, 0 for Encoder
    ## Collect Data Boolean
    C_Data = False              # Collect data?
    
    #Initialize the Shares
    ## Motor Fault Share Object
    fault = task_share.Share('i',  name = 'fault')
    
    ## Define the interrupt function (CONTAINS A SHARE VARIABLE)
    def faultthrow(line):
        '''
        @brief This function calls when the nFault pin, pin B2, is low, indicating an overcurrrent condition in the motor.
        '''
        motorX.disable()
        motorY.disable()
        fault.put(1)
        
    # Define external interupt to stop motors if there is a fault according to
    # the motor's internal fault pin.
    ## External Interrupt on the Two Motors nFAULT Pin (on a falling pin, throw interrupt)
    extint = pyb.ExtInt(pyb.Pin(pyb.Pin.board.PB2, mode=pyb.Pin.IN),
                        pyb.ExtInt.IRQ_FALLING, 
                        pyb.Pin.PULL_NONE, 
                        callback = faultthrow)
    
    extint.disable()  #disable interrupt while enabling motors
    motorY.enable()
    motorX.enable()
    
    
    #Setting up the Queues
    #X Queues
    ## X Queue
    X = task_share.Queue('f', 8, overwrite=True, name='X')
    ## X_dot Queue
    X_dot = task_share.Queue('f', 8, overwrite=True, name='X_dot')
    ## Theta Y Queue
    ThY = task_share.Queue('f', 8, overwrite=True, name='ThY')
    ## Theta Y_dot Queue
    ThY_dot = task_share.Queue('f', 8, overwrite=True, name='ThY_dot')
    
    #Y Queues
    ## Y Queue
    Y = task_share.Queue('f', 8, overwrite=True, name='Y')
    ## Y_dot Queue
    Y_dot = task_share.Queue('f', 8, overwrite=True, name='Y_dot')
    ## Theta X Queue
    ThX = task_share.Queue('f', 8, overwrite=True, name='ThX')
    ## Theta X_dot Queue
    ThX_dot = task_share.Queue('f', 8, overwrite=True, name='ThX_dot')
    
    if Runx:
        ## A cotask task object representing the x axis controller
        task1 = cotask.Task (xTable, name = 'xTable', priority = 5, 
                             period = 2.5)
        cotask.task_list.append (task1)
        
    if Runy:
        ## A cotask task object representing the y axis controller
        task2 = cotask.Task (yTable, name = 'yTable', priority = 6, 
                             period = 2.5)
        cotask.task_list.append (task2)
    
    if not EI:
        ## A cotask task object representing the encoder update function
        task3 = cotask.Task (chkEnc, name = 'chkEnc', priority = 3, 
                             period = 5)
        cotask.task_list.append (task3)
    
    if EI:
        ## A cotask task object representing the IMU update function
        task4 = cotask.Task (chkIMU, name = 'chkIMU', priority = 3, 
                             period = 5)
        cotask.task_list.append (task4)
    ## A cotask task object representing the touch panel update function    
    task5 = cotask.Task (chkTch, name = 'chkTch', priority = 4,
                         period = 2.5)
    cotask.task_list.append(task5)
    
    
    if C_Data:
        ## A cotask task object representing the store data to a csv file function
        task6 = cotask.Task(storeData, name = 'storeData', priority = 3, 
                            period = 100)
        cotask.task_list.append(task6)
        print('Collect Data Task Created')
   

    #Clean up memory before executing tasks
    gc.collect()
    
    ## Establish the variable to allow control to quit on any keyboard input if connected to REPL
    vcp = pyb.USB_VCP()
    ## Fault clearing input initial definition
    In = ''
    #Reenable interrupts for while loop
    extint.enable()
    #Run the scheduler with Dr. Ridgely's priority scheduling algorithm, quit on
    #any keyboard input.
    while not vcp.any():
        if fault.get():
            extint.disable()
            # User interface to deal with motor faults
            In = ''
            while In != 'Fault Cleared':
                In = input('Type "Fault Cleared" to resume motors if hardware is clear.\n')
    
            motorX.enable()
            motorY.enable()
            extint.enable()
            fault.put(0)
            
                    
                        
        cotask.task_list.pri_sched()
        
    #Empty comm port buffer
    vcp.read()
    
    motorX.disable()
    motorY.disable()
    
    #Print goodbye message
    print("Ba-da Ba Ba-Da Ba, BA!")