'''
@file HW1.py
@brief This file contains a function that calculates change in the fewest coins possible.
@details This file contains a function that takes the price of an item as an integer in cents and
         a tuple containing the payment method in twenties, tens, fives, ones, quarters, dimes, nickles, and pennies
         and returns a tuple containing the number of twenties, tens, fives, ones, quarters,
         dimes, nickels, and pennies that represent the fewest pieces for the change off an
         item.
@author Kai Quizon
@date January 13, 2021
'''

def getChange(price, payment):
    '''
    @brief This function calculates change in the fewest coins possible for a given price and payment scheme.
    @details This file contains a function that takes the price of an item as an integer in cents and
             a tuple containing the payment method in twenties, tens, fives, ones, quarters, dimes, nickles, and pennies
             and returns a tuple containing the number of twenties, tens, fives, ones, quarters,
             dimes, nickels, and pennies that represent the fewest pieces for the change off an
             item.
    @param price An integer value representing the price in cents (ex: $4.57 = 457)
    @param payment A tuple representing the payment method in ascending order.
    @return This function returns a tuple containing the change required to balance the books.
    '''
    
    # Handle invalid price entries
    if price < 0:
        print('Please enter a positive integer as the price')
        quit()
    if isinstance(price, int) == False:
        print('Please enter a positive integer as the price')
        quit()
    
    # Convert Payment to integer value
    paid = payment[0] + 5*payment[1] + 10*payment[2] + 25*payment[3] + 100*payment[4] + 500*payment[5] + 1000*payment[6] +2000*payment[7]
    
    
    # Handle insufficient payment
    if paid<price:
        print('Please provide a minimum payment tuple to cover the price of the item.')
        quit()
        
    # Compute Change Loop
    diff = paid - price
    change = [0, 0, 0, 0, 0, 0, 0, 0]
    while diff > 0:
        if diff%2000 == 0:
            change[7] += 1
            diff = diff-2000
        elif diff%1000 == 0:
            change[6] += 1
            diff = diff-1000
        elif diff%500 == 0:
            change[5] += 1
            diff = diff-500    
        elif diff%100 == 0:
            change[4] += 1
            diff = diff-100
        elif diff%25 == 0:
            change[3] += 1
            diff = diff-25
        elif diff%10 == 0:
            change[2] += 1
            diff = diff-10
        elif diff%5 == 0:
            change[1] += 1
            diff = diff-5
        elif diff%1 == 0:
            change[0] += 1
            diff = diff-1
    
    change = tuple(change)
    print(change)
    return change

if __name__ == '__main__':
    getChange(1500, (0,0,0,0,0,0,0,1))