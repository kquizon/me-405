'''
@file Lab7test.py
@brief A Test file for checking the functionality of the RTP.
@author Kai Quizon
'''

import pyb
import utime
import array

#This code is hard coded to the pins. The driver will require pin inputs to function

ym = pyb.Pin.cpu.A0
xm = pyb.Pin.cpu.A1
yp = pyb.Pin.cpu.A6
xp = pyb.Pin.cpu.A7

class touchtest:
    '''
    @brief A class to test the functionality of the RTP
    @details This class operates a resistave touch panel. The outputs of this script
             are uncalibrated and meant to ensure that the touch panel is working as intend.
             
             
    '''
    
    def __init__(self):
        
        
        ## Initiate buffer arrays to employ moving average reading filter
        self.xbuffy = array.array('h', [0, 0, 0])
        self.ybuffy = array.array('h', [0, 0, 0])
        self.zbuffy = array.array('h', [0, 0, 0])
        
        ## Initialize pins for touch device. ***NOTE: In this sample class, the pins are hard coded in.
        self.ym = pyb.Pin.board.PA7
        self.xm = pyb.Pin.board.PA6
        self.yp = pyb.Pin.board.PA1
        self.xp = pyb.Pin.board.PA0
        
        ## This variable stores an integer representing the most recent method called. It is used for optimizing pin declarations.
        self.state = 0
        
    
        
    #METHOD 1: Read the x location ONLY
    def xreader(self):
        '''
        @brief Read the x location of the touch panel ONLY.
        @details This method reads the touch panel's "x" output only. The reading is filtered
                 by taking the average of three readings. Settling time errors are eliminated
                 by introducing a delay of 5 us.
        @return The x value of the touch location in [mm].
        '''
        
        #These major if statements check the way the pins are currently configured.
        if self.state == 0 or self.state == 2 or self.state == 4: #NO pins properly declared
            
            ## Configure High Pin and set to High
            self.highpin = pyb.Pin(self.xm, mode=pyb.Pin.OUT_PP)
            self.highpin.high()
            
            ## Configure Low Pin and set to Low
            self.lowpin = pyb.Pin(self.xp, mode=pyb.Pin.IN)
            self.lowpin.low()
            
            ## Configure Floating Pin
            self.floater = pyb.Pin(self.yp, mode=pyb.Pin.IN)
            
            ## Configure ADC (read) Pin
            self.reader = pyb.ADC(self.ym)
            
            #Set new state value
            self.state = 1
        
        elif self.state == 3: #LOW and READ pins properly set.
        
            ## Configure High Pin and set to High
            self.highpin = pyb.Pin(self.xm, mode=pyb.Pin.OUT_PP)
            self.highpin.high()
            
            
            ## Configure Floating Pin
            self.floater = pyb.Pin(self.yp, mode=pyb.Pin.IN)
            
            #Set new state value
            self.state = 1
            
        
        #Take 3 samples from touch plate
        utime.sleep_us(5)
        self.xbuffy[0] = self.reader.read()
        
        utime.sleep_us(5)
        self.xbuffy[1] = self.reader.read()
        
        utime.sleep_us(5)
        self.xbuffy[2] = self.reader.read()
        
        #'Filter' Data by taking average of 3 data points
        xposition = 0.0524*(round((self.xbuffy[0]+self.xbuffy[1]+self.xbuffy[2])/3)-2065)
        
        return xposition
    
    #METHOD 2: Read y position ONLY
    def yreader(self):
        '''
        @brief Read the y location of the touch panel ONLY.
        @details This method reads the touch panel's "x" output only. The reading is filtered
                 by taking the average of three readings. Settling time errors are eliminated
                 by introducing a delay of 5 us.
        @return The y value of the touch location in [mm].
        '''
        
        #These major if statements check the way the pins are currently configured.
        if self.state == 0 or self.state == 1 or self.state == 3: #NO pins properly declared
            
            ## Configure High Pin and set to High
            self.highpin = pyb.Pin(self.yp, mode=pyb.Pin.OUT_PP)
            self.highpin.high()
            
            ## Configure Low Pin and set to Low
            self.lowpin = pyb.Pin(self.ym, mode=pyb.Pin.IN)
            self.lowpin.low()
            
            ## Configure Floating Pin
            self.floater = pyb.Pin(self.xp, mode=pyb.Pin.IN)
            
            ## Configure ADC (read) Pin
            self.reader = pyb.ADC(self.xm)
            
            #Set new state value
            self.state = 2
        
        elif self.state == 3: #LOW and READ pins properly set.
        
            ## Configure High Pin and set to High
            self.highpin = pyb.Pin(self.xm, mode=pyb.Pin.OUT_PP)
            self.highpin.high()
            
            
            ## Configure Floating Pin
            self.floater = pyb.Pin(self.yp, mode=pyb.Pin.IN)
            
            #Set new state value
            self.state = 2
            
        
        #Take 3 samples from touch plate
        utime.sleep_us(5)
        self.ybuffy[0] = self.reader.read()
        
        utime.sleep_us(5)
        self.ybuffy[1] = self.reader.read()
        
        utime.sleep_us(5)
        self.ybuffy[2] = self.reader.read()
        
        #'Filter' Data by taking average of 3 data points
        yposition = -0.0345*(round((self.ybuffy[0]+self.ybuffy[1]+self.ybuffy[2])/3)-1950)
        
        return yposition
        
    #METHOD 3: Read z direction ONLY
    def zreader(self):
        '''
        @brief Read the y location of the touch panel ONLY.
        @details This method reads the touch panel's "x" output only. The reading is filtered
                 by taking the average of three readings. Settling time errors are eliminated
                 by introducing a delay of 5 us.
        @return The z value of the touch location in [mm].
        @return A boolean flag on whether the plate is being contacted or not.
        '''
        
        #These major if statements check the way the pins are currently configured.
        if self.state == 0: #NO pins properly declared
            
            ## Configure High Pin and set to High
            self.highpin = pyb.Pin(self.yp, mode=pyb.Pin.OUT_PP)
            self.highpin.high()
            
            ## Configure Low Pin and set to Low
            self.lowpin = pyb.Pin(self.xp, mode=pyb.Pin.IN)
            self.lowpin.low()
            
            ## Configure Floating Pin
            self.floater = pyb.Pin(self.ym, mode=pyb.Pin.IN)
            
            ## Configure ADC (read) Pin
            self.reader = pyb.ADC(self.xm)
            
            #Set new state value
            self.state = 3
        
        elif self.state == 2: #HIGH and READ pins properly set.
        
            ## Configure Low Pin and set to Low
            self.highpin = pyb.Pin(self.xp, mode=pyb.Pin.OUT_PP)
            self.highpin.high()
            
            
            ## Configure Floating Pin
            self.floater = pyb.Pin(self.ym, mode=pyb.Pin.IN)
            
            #Set new state value
            self.state = 3
            
        elif self.state == 1: #ADC and LOW pins properly set.
            
            ## Configure High Pin and set to High
            self.highpin = pyb.Pin(self.yp, mode=pyb.Pin.OUT_PP)
            self.highpin.high()

            
            ## Configure Floating Pin
            self.floater = pyb.Pin(self.ym, mode=pyb.Pin.IN)
             
            #Set new state value
            self.state = 3
        
        #Take 3 samples from touch plate
        utime.sleep_us(5)
        self.zbuffy[0] = self.reader.read()
        
        utime.sleep_us(5)
        self.zbuffy[1] = self.reader.read()
        
        utime.sleep_us(5)
        self.zbuffy[2] = self.reader.read()
        
        #'Filter' Data by taking average of 3 data points
        zposition = round((self.ybuffy[0]+self.ybuffy[1]+self.ybuffy[2])/3)
        
        #Turn into Boolean true/false
        if 10 < zposition < 4080:
            zflag = True
        else:
            zflag = False
            
        return zposition, zflag
    
    #METHOD 4: Read ALL 3 Channels
    def read(self):
        '''
        @brief Read the x,y, and z locations of the touch panel.
        @details This method reads the touch panel's three outputs. The reading is filtered
                 by taking the average of three readings. Settling time errors are eliminated
                 by introducing a delay of 5 us.
        @return The x,y, and z values of the touch location in [mm] as a tuple. Unless there is no contact, then the tuple contains all logical Falses.
        '''
        
        [zread, booltouch] = self.zreader()
        
        if booltouch == True:
            xread = self.xreader()
            yread = self.yreader()
        else:
            xread = False
            yread = False
            
        coords = (xread,yread,zread)
        
        return coords
    
            
if __name__ == '__main__':
    while True:
        try:
            test = input('\n'*20 + 'What direction would you like to test?\n     - For X direction, enter "x"\n     -For Y direction, enter "y"\n     -For contact test, enter "z"\n     -For full-system test, enter "Full"\n\n                    >>>     ')
            if test != 'x' and test != 'y' and test != 'z' and test != 'Full':
                test = 'Full'

            
            tch = touchtest()
            # dsp = touchDisp.touchDisp()
            
            if test == 'x':
                while True:
                    try:
                        t0 = utime.ticks_us()
                        xpos = tch.xreader()
                        
                        t1 = utime.ticks_us()
                        tdif = utime.ticks_diff(t1,t0)
                        
                        print('\n'*20 +' You are touching at: X = ' + str(xpos) + '\n')
                        print('Readtime: ' + str(tdif) + ' us' + '\n'*5)
                        utime.sleep_ms(100)
                    except KeyboardInterrupt:
                        break  
            elif test == 'y':
                while True:
                    try:
                        t0 = utime.ticks_us()
                        ypos = tch.yreader()
                        
                        t1 = utime.ticks_us()
                        tdif = utime.ticks_diff(t1,t0)
                        
                        print('\n'*20 +' You are touching at: Y = ' + str(ypos) + '\n')
                        print('Readtime: ' + str(tdif) + ' us' + '\n'*5)
                        utime.sleep_ms(100)
                    except KeyboardInterrupt:
                        break  
            elif test == 'z':
                while True:
                    try:
                        t0 = utime.ticks_us()
                        [zpos, zflag] = tch.zreader()
                        if zflag == True:
                            t1 = utime.ticks_us()
                            tdif = utime.ticks_diff(t1,t0)
                            
                            print('\n' * 20 + 'You are touching the screen' + '\n')
                            print('Readtime: ' + str(tdif) + ' us' + '\n'*5)
                        else:
                            print('\n' * 20 + 'You are not touching the screen' + '\n'*7)
                        utime.sleep_ms(100)
                    except KeyboardInterrupt:
                        break  
            elif test == 'Full':
                while True:
                    try:
                        
                        if zflag == False:
                            t0 = utime.ticks_us()
                            [zpos, zflag] = tch.zreader()
                            t1 = utime.ticks_us()
                            tdif = utime.ticks_diff(t1,t0)
                        if tch.tch == True:
                            t0 = utime.ticks_us()
                            position = tch.read()
                            t1 = utime.ticks_us()
                            tdif = utime.ticks_diff(t1,t0)
                            
                            print('\n'*20)
                            
                            # dsp.disp(tch)
                            
                            print('\n'*2 +' You are touching at: X = ' + str(position[0]) +'     Y = ' + str(position[1]) + '     Z = ' + str(position[2]) + ' \n')
                            print('Readtime: ' + str(tdif) + ' us' + '\n'*5)
                        else:
                            print('\n'*20)
                            # dsp.disp(tch)
                            print('\n' * 2 + 'You are not touching the screen' + '\n'*2)
                            print('Readtime: ' + str(tdif) + ' us' + '\n'*5)
                        utime.sleep_ms(100)
                    except KeyboardInterrupt:
                        break  
        except KeyboardInterrupt:
            print('\n'*50 + 'Sucessfuly Exited Test Program \n\n')
            break 
